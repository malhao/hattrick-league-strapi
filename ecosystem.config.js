module.exports = {
  apps: [
    {
      name: 'strapi',
      cwd: '/var/www/api.hattrickleague.com',
      script: 'npm',
      args: 'start',
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
